## Installation

Add this line to your application's Gemfile:

```ruby
gem 'selectel_uploader'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install selectel_uploader

## Usage

Для начала работы с гемом добавьте файл `config/inizializers/selectel_uploader.rb` с объявлением настроек
```
SelectelUploader.configure do |config|
  config.user = 'user'
  config.password = 'password'
  config.container = '77777'
  config.url = 'https://auth.selcdn.ru'
  config.path = 'production'
end

```

Для загрузки файла доступен метод `upload`, который вызывается следующим образом:

```
SelectelUploader::Client.upload(io, filename)

```
который возвращает ссылку на загруженный файл
