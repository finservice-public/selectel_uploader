require 'http'
require 'selectel_uploader/version'
require 'selectel_uploader/authentication'
require 'selectel_uploader/client'
require 'selectel_uploader/client/url_builder'

module SelectelUploader
  class InvalidToken  < StandardError; end
  class UploadError   < StandardError; end
  class ConfigMissing < StandardError; end

  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
    configuration
  end

  class Configuration
    CONFIGURATION_FIELDS = %w(user password url container path).freeze
    attr_writer *CONFIGURATION_FIELDS

    CONFIGURATION_FIELDS.each do |field|
      define_method field do
        field_value = instance_variable_get("@#{field}")
        raise ConfigMissing.new("config #{field} is missing") if field_value.to_s.empty?
        field_value
      end
    end

    def authtoken
      @authtoken ||= Authentication.new.authenticate!
    end

    def reset_authtoken!
      @authtoken = nil
    end
  end
end
