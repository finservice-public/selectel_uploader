module SelectelUploader
  class Authentication

    def authenticate!
      tries = 2
      begin
        result = HTTP.post("#{SelectelUploader.configuration.url}/v2.0/tokens", body: auth_data.to_json)
        JSON.parse(result).dig('access', 'token', 'id')
      rescue => e
        retry unless (tries -= 1) <= 0
      end
    end

    private

    def auth_data
      {
        auth: {
          passwordCredentials: {
            username: SelectelUploader.configuration.user,
            password: SelectelUploader.configuration.password
          }
        }
      }
    end
  end
end
