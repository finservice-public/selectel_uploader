# frozen_string_literal: true

require 'uri'

module SelectelUploader
  class Client
    class UrlBuilder
      attr_reader :filename

      def initialize(filename)
        @filename = filename
      end

      def build
        URI.escape(url)
      end

      private

      def url
        "#{SelectelUploader.configuration.url}/v1/SEL_#{SelectelUploader.configuration.container}/#{SelectelUploader.configuration.path}/#{filename}"
      end
    end
  end
end
