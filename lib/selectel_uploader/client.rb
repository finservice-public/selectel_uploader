# frozen_string_literal: true

module SelectelUploader
  class Client
    def self.upload(io, filename)
      tries = 2

      begin
        url = UrlBuilder.new(filename).build
        request = HTTP.headers('x-auth-token' => SelectelUploader.configuration.authtoken).put(url, body: io)
        raise InvalidToken.new, 'Ошибка авторизации' if request.code == 401
        raise UploadError.new, "Ошибка загрузки, код ответа: #{request.code}" unless request.status.success?
        url
      rescue InvalidToken
        SelectelUploader.configuration.reset_authtoken!
        retry unless (tries -= 1) <= 0
        raise e
      rescue StandardError => e
        retry unless (tries -= 1) <= 0
        raise e
      end
    end
  end
end
