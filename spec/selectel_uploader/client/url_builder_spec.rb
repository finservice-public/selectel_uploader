# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SelectelUploader::Client::UrlBuilder do
  subject(:instance) { described_class.new(filename) }

  describe '#build' do
    subject { instance.build }
    let(:configuration) { instance_double SelectelUploader::Configuration, url: 'https://lvh.me', container: 'CONTAINER', path: 'PATH' }

    before do
      allow(SelectelUploader).to receive(:configuration).and_return(configuration)
    end

    context 'when filename contains unescaped symbols' do
      let(:filename) { 'file/☣.html' }

      it 'filename contains unescaped symbols' do
        expect(URI.escape(filename)).not_to eq(filename)
      end

      it 'returns escaped URL' do
        url = "#{SelectelUploader.configuration.url}/v1/SEL_#{SelectelUploader.configuration.container}/#{SelectelUploader.configuration.path}/#{filename}"
        expected_url = URI.escape(url)
        is_expected.to eq(expected_url)
      end
    end
  end
end
