# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SelectelUploader::Client do
  subject(:upload) { described_class.upload(io, filename) }

  let(:io) { File.new('spec/fixtures/test_upload.txt') }
  let(:filename) { 'test_upload.txt' }
  let(:configuration) do
    instance_double(SelectelUploader::Configuration, url: 'https://lvh.me', container: 'CONTAINER', path: 'PATH', authtoken: 'auth')
  end
  let(:response_code)    { 500 }
  let(:response_success) { false }
  let(:response) do
    double(code: response_code, status: OpenStruct.new(success?: response_success))
  end

  before do
    allow(SelectelUploader).to receive(:configuration).and_return(configuration)
    allow(HTTP).to receive_message_chain(:headers, :put).and_return(response)
  end

  it 'raise SelectelUploader::UploadError if X times not 200' do
    expect { upload }.to raise_error(SelectelUploader::UploadError)
  end

  context 'when upload success' do
    let(:response_code)    { 200 }
    let(:response_success) { true }

    it 'does not raise error' do
      expect { upload }.not_to raise_error
    end

    it 'returns url' do
      expect(upload).to be_kind_of(String)
    end
  end
end
